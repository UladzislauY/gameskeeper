//
//  AddPlayerViewController.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import UIKit

class AddPlayerViewController: UIViewController {
    
    //MARK: - Constants
    
    private enum Constant {
        
        static let screenTitle = "Add Player"
        static let textFieldPlaceholder = "Player Name"
        
        enum Size {
            static let defaultValue: CGFloat = 0
            static let addPlayerTextFieldHeight: CGFloat = 60
        }
    }
    
    //MARK: - Variables
    
    public var presenter: AddPlayerViewPresenterProtocol!
    
    //MARK: - UserInterface Elements
    
    private lazy var addPlayerTextField = AddPlayerTextField(placeholder: Constant.textFieldPlaceholder, frame: .zero, delegate: self)
    private lazy var leftBarButtonItem = NavigationBarButton(imageName: Constants.Buttons.back, execute: presenter.popToRootViewController)
    private lazy var rightBarButtonItem = NavigationBarButton(imageName: Constants.Buttons.add, execute: addPlayer)
    
    //MARK: - ViewController Life Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constant.screenTitle
        self.view.backgroundColor = UIColor.colorize(r: 35, g: 35, b: 35, a: 1)
        self.customizeNavigationBar()
        self.setConstraints()
        self.view.addSubview(addPlayerTextField)
        
    }
    
    private func setConstraints() {
        
        self.addPlayerTextField.activateConstraintsOnView(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: .init(), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.addPlayerTextFieldHeight))
    }
    
    private func customizeNavigationBar() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.rightBarButtonItem)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.leftBarButtonItem)
        self.customizeNavigationBar(navigationController: self.navigationController ?? UINavigationController(), hasLargeTitle: true)
    }
    
    @objc private func addPlayer() {
        self.checkIfTextFieldInputIsEmpty(input: self.addPlayerTextField.text ?? "", isActive: self.rightBarButtonItem.isEnabled)
    }
    
    @objc private func popViewController() {
        self.popToRootScreen()
    }
}

//MARK: - Extenions
extension AddPlayerViewController: AddPlayerViewProtocol {

    func popToRootScreen() {
        presenter.popToRootViewController()
    }
    
    func checkIfTextFieldInputIsEmpty(input: String, isActive: Bool) {
        presenter.givenInput(input: input, isActive: isActive)

    }
}

extension AddPlayerViewController: UITextFieldDelegate {
    

}
