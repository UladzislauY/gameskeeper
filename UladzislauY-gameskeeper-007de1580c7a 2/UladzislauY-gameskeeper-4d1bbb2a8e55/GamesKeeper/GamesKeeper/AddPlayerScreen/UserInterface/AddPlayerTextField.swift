//
//  AddPlayerTextField.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import UIKit

class AddPlayerTextField: UITextField {
    
    private let placeholderPadding = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
    
    init(placeholder: String, frame: CGRect, delegate: UITextFieldDelegate?) {
        
        super.init(frame: frame)
        self.placeholder = placeholder
        self.backgroundColor = UIColor.setCustomColor(color: .mainColor)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.delegate = delegate
        self.setupTextFieldPlaceholder(placeholder: placeholder)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupTextFieldPlaceholder(placeholder: String) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorize(r: 155, g: 155, b: 161, a: 1), NSAttributedString.Key.font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: 24) ?? UIFont()])
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.placeholderPadding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.placeholderPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.placeholderPadding)
    }
}
