//
//  AddPlayerViewPresenterProtocol.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import Foundation

protocol AddPlayerViewPresenterProtocol: AnyObject {
    
    func givenInput(input: String, isActive: Bool)
    func popToRootViewController()
}
