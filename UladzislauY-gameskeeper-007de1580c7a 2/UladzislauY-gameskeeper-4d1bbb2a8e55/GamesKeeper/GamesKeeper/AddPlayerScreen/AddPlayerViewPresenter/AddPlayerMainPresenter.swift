//
//  AddPlayerMainPresenter.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import Foundation

class AddPlayerMainPresenter: AddPlayerViewPresenterProtocol {
    
    private weak var addPlayerView: AddPlayerViewProtocol?
    private var model: Player
    private var router: RouterProtocol?
    
    init(view: AddPlayerViewProtocol, model: Player, router: RouterProtocol) {
        self.addPlayerView = view
        self.model = model
        self.router = router
    }
    
    func givenInput(input: String, isActive: Bool = false) {
        var active = isActive
        if input.isEmpty == false {
            active.toggle()
            self.model.add(player: input)
            self.popToRootViewController()
            self.router?.givenInput(input: input)
        }
    }
    
    func popToRootViewController() {
        self.router?.navigate(with: .popToRoot)
    }
}
