//
//  AddPlayerViewProtocol .swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import Foundation

protocol AddPlayerViewProtocol: AnyObject {
    
    func checkIfTextFieldInputIsEmpty(input: String, isActive: Bool)
//    func popToRootScreen()
   
}
