//
//  Dice.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import UIKit

enum Dice: Int, CaseIterable {
    
    case one = 1, two, three, four, five, six
    
}
