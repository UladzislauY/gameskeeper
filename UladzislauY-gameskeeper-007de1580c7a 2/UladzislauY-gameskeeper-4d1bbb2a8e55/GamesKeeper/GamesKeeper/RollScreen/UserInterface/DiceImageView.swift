//
//  DiceImageView.swift
//  GamesKeeper
//
//  Created by admin on 2.12.21.
//

import Foundation
import UIKit

class DiceImageView: UIImageView {
    
    private enum Constant {
        
        static let diceFrame: CGRect = CGRect(x: 0, y: 0, width: 100, height: 100)
        static let diceOne = "diceOne"
        static let diceTwo = "diceTwo"
        static let diceThree = "diceThree"
        static let diceFour = "diceFour"
        static let diceFive = "diceFive"
        static let diceSix = "diceSix"
    }
    
    init(value: Dice) {
        super.init(frame: Constant.diceFrame)
        switch value {
        case .one:
            image = UIImage(named: Constant.diceOne)
        case .two:
            image = UIImage(named: Constant.diceTwo)
        case .three:
            image = UIImage(named: Constant.diceThree)
        case .four:
            image = UIImage(named: Constant.diceFour)
        case .five:
            image = UIImage(named: Constant.diceFive)
        case .six:
            image = UIImage(named: Constant.diceSix)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
