//
//  RollViewController.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import UIKit

class RollViewController: UIViewController {
    //MARK: - Constants
    enum Constant {
        static let repeatCount: Float = 1
        static let duration: CFTimeInterval = 1.0
    }
    //MARK: - Variables
    public var presenter: RollViewPresenterProtocol!
    
    //MARK: - Life cycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.setCustomColor(color: .mainColor)
        self.navigationItem.hidesBackButton = true
        self.view.addBlurEffect(to: self.view, style: .extraLight)
        popToGameProcessScreen()
        rollDice()
    }
    
    private func popToGameProcessScreen() {
        let tap: UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapGoBackToGamesProcessScreen))
        self.view.addGestureRecognizer(tap)
    }
}

//MARK: - Extensions 
extension RollViewController: RollViewProtocol {
    
    @objc func onTapGoBackToGamesProcessScreen() {
        self.presenter.popToProcessGameScreen()
    }
    
    func rollDice() {
        guard let dice = presenter.roll() else {return}
        let diceView = DiceImageView(value: dice)
        diceView.frame.origin = CGPoint(x: (self.view.frame.origin.x + self.view.frame.width / 2) - 50, y: self.view.frame.origin.y - 100)
        self.view.addSubview(diceView)
        diceView.animate(repeatCount: Constant.repeatCount, duration: Constant.duration)
    }
}
