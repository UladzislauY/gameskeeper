//
//  RollViewProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

protocol RollViewProtocol: AnyObject {
    
    func rollDice()
    func onTapGoBackToGamesProcessScreen()
}
