//
//  RollViewPresenterProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

protocol RollViewPresenterProtocol: AnyObject {
    
    func roll() -> Dice?
    func popToProcessGameScreen()
}
