//
//  RollViewMainPresenter.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

class RollViewMainPresenter: RollViewPresenterProtocol {
    
    weak var rollView: RollViewProtocol?
    var router: RouterProtocol?
    
    init(rollView: RollViewProtocol, router: RouterProtocol) {
        self.rollView = rollView
        self.router = router
    }
    
    func roll() -> Dice? {
        return Dice.allCases.randomElement()
    }
    
    func popToProcessGameScreen() {
        router?.navigate(with: .gameProcessScreen)
    }
}
