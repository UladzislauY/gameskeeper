//
//  NewGameScreenBuilder.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import UIKit

protocol ModuleBuilderProtocol {
    
    func createNewGameScreen(router: RouterProtocol) -> UIViewController
    func createAddPlayerScreen(router: RouterProtocol) -> UIViewController
    func createResultScreen(router: RouterProtocol) -> UIViewController
    func createGameProcessScreen(router: RouterProtocol) -> UIViewController
    func createRollScreen(router: RouterProtocol) -> UIViewController
}

class MainModuleBuilder: ModuleBuilderProtocol {
    
    func createNewGameScreen(router: RouterProtocol) -> UIViewController {
        
        let newGameView = NewGameViewController()
        let playerModel = Player()
        let persistance = PersistanceManager.shared
        let newGameViewPresenter = NewGameMainPresenter(view: newGameView, model: playerModel, router: router, persistance: persistance)
        newGameView.presenter = newGameViewPresenter
        return newGameView
        
    }
    
    func createAddPlayerScreen(router: RouterProtocol) -> UIViewController {
        
        let playerModel = Player()
        let addPlayerViewController = AddPlayerViewController()
        let presenter = AddPlayerMainPresenter(view: addPlayerViewController, model: playerModel, router: router)
        addPlayerViewController.presenter = presenter
        return addPlayerViewController
    }
    
    func createResultScreen(router: RouterProtocol) -> UIViewController {
        
        let playerModel = Player()
        let resultView = ResultViewController()
        let resultViewPresenter = ResultViewMainPresenter(resultView: resultView, playerModel: playerModel, router: router)
        resultView.presenter = resultViewPresenter
        return resultView
    }
    
    func createGameProcessScreen(router: RouterProtocol) -> UIViewController {
        
        let player = Player()
        let point = Point()
        let persistance = PersistanceManager.shared
        let gameProcessView = GameProcessViewController()
        let gameProcessViewPresenter = GameProcessMainPresenter(gameProcessView: gameProcessView, player: player, point: point, router: router, persistance: persistance)
        gameProcessView.presenter = gameProcessViewPresenter
        return gameProcessView
    }
    
    func createRollScreen(router: RouterProtocol) -> UIViewController {

        let rollView = RollViewController()
        let rollViewMainPresenter = RollViewMainPresenter(rollView: rollView, router: router)
        rollView.presenter = rollViewMainPresenter
        return rollView
    }
}
