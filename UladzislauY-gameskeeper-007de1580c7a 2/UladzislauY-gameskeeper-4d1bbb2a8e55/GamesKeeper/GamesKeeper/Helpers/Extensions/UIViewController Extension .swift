//
//  UINavigationController Extension .swift
//  GamesKeeper
//
//  Created by Владислав on 11/22/21.
//

import UIKit

extension UIViewController {
    
    func customizeNavigationBar(navigationController: UINavigationController, hasLargeTitle: Bool) {
        
        navigationController.navigationBar.prefersLargeTitles = hasLargeTitle
        navigationController.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont(name: Constants.CustomFonts.nunitoBold, size: 41) ?? UIFont()]
    }
}
