//
//  UIImageView Extension.swift
//  GamesKeeper
//
//  Created by Владислав on 12/3/21.
//

import UIKit

fileprivate enum AnimationKey {
    
    static let transformRotation = "transform.rotation.z"
    static let rotationAnimation = "rotationAnimation"
    static let changeYPosition = "position"
    static let radiantValue: Double = Double.pi * 2
}

extension UIImageView {
    
    func animate(repeatCount: Float, duration: CFTimeInterval) {
        
        let rotation: CABasicAnimation = CABasicAnimation(keyPath: AnimationKey.transformRotation)
        rotation.duration = duration
        rotation.repeatCount = repeatCount
        rotation.isCumulative = true
        rotation.toValue = NSNumber(value: AnimationKey.radiantValue)
        UIView.animate(withDuration: duration) {
            self.frame.origin.y += 500
            self.layer.add(rotation, forKey: AnimationKey.rotationAnimation)
        }
    }
}
