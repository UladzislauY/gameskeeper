//
//  UIColor Extension .swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import UIKit

enum CustomColor{
    case mainColor
    case currentPlayerCollectionViewColor
    case defaultColor
}

extension UIColor {
    
    static func colorize(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
    static func setCustomColor(color: CustomColor) -> UIColor? {
        switch color {
        case .mainColor:
            return UIColor(named: "mainColor")
        case .currentPlayerCollectionViewColor:
            return UIColor(named: "currentPlayerCollectionViewColor")
        case .defaultColor:
            return UIColor(named: "defaultColor")
        }
    }
}
