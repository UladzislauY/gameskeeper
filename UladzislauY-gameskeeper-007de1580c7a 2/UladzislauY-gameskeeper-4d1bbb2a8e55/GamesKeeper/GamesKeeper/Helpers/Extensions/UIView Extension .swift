//
//  UIView Extension .swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import UIKit

extension UIView {
    
    func activateConstraintsOnView(top: NSLayoutYAxisAnchor?,
                                   leading: NSLayoutXAxisAnchor?,
                                   bottom: NSLayoutYAxisAnchor?,
                                   trailing: NSLayoutXAxisAnchor?,
                                   padding: UIEdgeInsets = .zero,
                                   size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
            
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
    
    func addBlurEffect(to targetView: UIView, style: UIBlurEffect.Style = .light) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.frame = targetView.bounds
        targetView.addSubview(blurEffectView)
        
    }
}


