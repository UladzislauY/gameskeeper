//
//  Constants.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import Foundation

enum Constants {
    
    enum CustomFonts {
        
        static let nunitoRegular = "Nunito-Regular"
        static let nunitoBold = "Nunito-Bold"
        
    }
    
    enum Buttons {
        
        static let diceButton = "diceButton"
        static let cancelButton = "cancelButton"
        static let backButton = "backButton"
        static let addButton = "addButton"
        static let resumeButton = "resumeButton"
        static let add = "add"
        static let back = "back"
        static let previousButton = "previousButton"
        static let nextButton = "nextButton"
        static let undo = "undo"
        static let pause = "pause"
        static let play = "play"
    }
    
    enum ButtonTitle {
        
        static let addButtonTitle = "Add Player"
    }
    
    enum Image {
        
        static let addButtonImage = "addButton"
        static let startGameButton = "startGameButton"
        
    }
    
    enum Title {
        
        static let headerTitle = " Add Players"
    }
}
