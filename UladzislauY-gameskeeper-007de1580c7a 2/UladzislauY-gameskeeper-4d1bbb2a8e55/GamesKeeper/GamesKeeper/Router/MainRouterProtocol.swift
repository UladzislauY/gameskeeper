//
//  MainRouter.swift
//  GamesKeeper
//
//  Created by Владислав on 11/21/21.
//

import UIKit

protocol MainRouterProtocol {
    
    var navigationController: UINavigationController? { get set }
    var moduleBuilder: ModuleBuilderProtocol? { get set }
}
