//
//  NavigationType.swift
//  GamesKeeper
//
//  Created by Владислав on 11/21/21.
//

import Foundation

enum NavigationType {
    
    case addPlayerScreen, popToRoot, resultScreen, gameProcessScreen, rollDiceScreen
}
