//
//  Router.swift
//  GamesKeeper
//
//  Created by Владислав on 11/21/21.
//

import UIKit

class Router: RouterProtocol {
    
    var navigationController: UINavigationController?
    var moduleBuilder: ModuleBuilderProtocol?
    
    init(navigationController: UINavigationController, builder: ModuleBuilderProtocol) {
        self.navigationController = navigationController
        self.moduleBuilder = builder
    }
    
    func initializeRootViewController() {
        if let navigationController = self.navigationController {
            guard let rootViewController = self.moduleBuilder?.createNewGameScreen(router: self) else {return}
            navigationController.viewControllers = [rootViewController]
        }
    }
    
    func givenInput(input: String) {
        (navigationController?.viewControllers.first(where: {$0 is NewGameViewController}) as? NewGameViewController)?.presenter.givenInput(input: input)
        print("Input is: \(input)")
    }
    
    func navigate(with navigationType: NavigationType) {
        
        switch navigationType {
        case .addPlayerScreen:
            if let navigationController = self.navigationController {
                guard let addPlayerViewController = self.moduleBuilder?.createAddPlayerScreen(router: self) else {return}
                navigationController.pushViewController(addPlayerViewController, animated: true)
            }
        case .popToRoot:
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
            }
        case .resultScreen:
            if let navigationController = self.navigationController {
                guard let resultViewController = self.moduleBuilder?.createResultScreen(router: self) else {return}
                navigationController.pushViewController(resultViewController, animated: true)
            }
        case .gameProcessScreen:
            if let navigationController = self.navigationController {
                guard let gameProcessViewController = self.moduleBuilder?.createGameProcessScreen(router: self) else {return}
                navigationController.pushViewController(gameProcessViewController, animated: true)
            }
        case .rollDiceScreen:
            if let navigationController = self.navigationController {
                guard let rollDiceScreen = self.moduleBuilder?.createRollScreen(router: self) else {return}
                navigationController.pushViewController(rollDiceScreen, animated: true)
            }
        }
    }
    
}

