//
//  RouterProtocol .swift
//  GamesKeeper
//
//  Created by Владислав on 11/21/21.
//

import UIKit

protocol RouterProtocol: MainRouterProtocol {
    
    func initializeRootViewController()
    func navigate(with navigationType: NavigationType)
    func givenInput(input: String)
}
