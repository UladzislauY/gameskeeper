//
//  ResultTableViewCell.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import UIKit

protocol ResultTableViewCellProtocol {
    func configureResultTableViewCell(with model: Player)
}

class ResultTableViewCell: NewGameTableViewCell {
    
    enum Constant {
        static let scoreLabelFontSize: CGFloat = 20
        static let playerNameLabelFontSize: CGFloat = 20
    }
    
    private lazy var scoreLabel = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: Constant.scoreLabelFontSize) ?? .init(), textColor: .white, alignment: .right)
    private lazy var playerNameLabel = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoBold, size:  Constant.playerNameLabelFontSize) ?? .init(), textColor: UIColor.white, alignment: .left)

    override var reuseIdentifier: String {
        return String(describing: self)
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.addSubview(scoreLabel)
        self.contentView.addSubview(playerNameLabel)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.playerNameLabel.activateConstraintsOnView(top: self.contentView.topAnchor, leading: self.contentView.leadingAnchor, bottom: self.contentView.bottomAnchor, trailing: nil, padding: .init(top: 15, left: 15, bottom: -15, right: 0), size: .init(width: 0, height: 24))
        
        self.scoreLabel.activateConstraintsOnView(top: self.contentView.topAnchor, leading: nil, bottom: self.contentView.bottomAnchor, trailing: self.contentView.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 15, right: -15), size: .init(width: 0, height: 24))
    }
}

extension ResultTableViewCell: ResultTableViewCellProtocol {
    
    func configureResultTableViewCell(with model: Player) {
        self.playerNameLabel.text = model.name
        self.scoreLabel.text = "\(model.score?.score ?? Int())"
    }
}
