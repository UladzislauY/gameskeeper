//
//  NewGameTableViewCell.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import UIKit

protocol NewGameTableViewCellProtocol {
    
    func configureNewGameTableViewCell(numeration: IndexPath, with model: Player)
}

class NewGameTableViewCell: UITableViewCell {
    
    //MARK: - Constants
    private enum Constant {
        static let playerNameLabelFontSize: CGFloat = 20
        
        enum Size {
            static let defaultValue: CGFloat = 0
            static let playerNameLabelHeight: CGFloat = 24
            static let playerNameLabelTopAnchor: CGFloat = 1
            static let playerNameLabelLeftAnchor: CGFloat = 1
            static let playerNameLabelRightAnchor: CGFloat = -1
        }
    }
    //MARK: - Variables
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    //MARK: - UserInterface Elements
    private lazy var playerNameLabel = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoBold, size: Constant.playerNameLabelFontSize) ?? .init(), textColor: UIColor.setCustomColor(color: .defaultColor) ?? .black, alignment: nil)
    //MARK: - Cell Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.backgroundColor = UIColor.setCustomColor(color: .mainColor)
        self.addSubview(self.playerNameLabel)
        self.playerNameLabel.activateConstraintsOnView(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: Constant.Size.playerNameLabelTopAnchor, left: Constant.Size.playerNameLabelLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.playerNameLabelRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.playerNameLabelHeight))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.playerNameLabel.text = nil 
    }
}

//MARK: - Extensions 
extension NewGameTableViewCell: NewGameTableViewCellProtocol {

    func configureNewGameTableViewCell(numeration: IndexPath, with model: Player) {
        self.playerNameLabel.text = "\(numeration.row + 1). \(model.name ?? "")"
    }
}
