//
//  GameProcessViewPresenterProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation



protocol GameProcessViewPresenterProtocol {
    
    func pushResultsScreen()
    func pushToRollDiceScreen()
    func pushToNewGameScreen()
    func makeUndo()
    func plusOnePoint()
    func plusFivePoints()
    func plusTenPoints()
    func minusOnePoint()
    func minusFivePoints()
    func minusTenPoints()
    func retrievePlayersFromDataBase() throws
    var players: [Player] {get set}
    
}
