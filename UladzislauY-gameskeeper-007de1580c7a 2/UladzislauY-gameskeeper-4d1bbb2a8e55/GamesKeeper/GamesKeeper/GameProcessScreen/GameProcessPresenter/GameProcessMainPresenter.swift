//
//  GameProcessMainPresenter.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

class GameProcessMainPresenter: GameProcessViewPresenterProtocol {
    
    weak var gameProcessView: GameProcessViewProtocol?
    let player: Player
    var router: RouterProtocol?
    var point: Point
    private var persistance: Persisting?
    public var players: [Player] = []
    
    init(gameProcessView: GameProcessViewProtocol, player: Player, point: Point, router: RouterProtocol, persistance: Persisting) {
        self.gameProcessView = gameProcessView
        self.player = player
        self.point = point
        self.router = router
    }
    
    func pushResultsScreen() {
        self.router?.navigate(with: .resultScreen)
    }
    
    func pushToRollDiceScreen() {
        self.router?.navigate(with: .rollDiceScreen)
    }
    
    func pushToNewGameScreen() {
        self.router?.navigate(with: .addPlayerScreen)
    }
    
    func makeUndo() {
        self.gameProcessView?.undo()
    }
    
    func plusOnePoint() {
        let incrementByOne = point.incrementPoint(for: 1)
        self.gameProcessView?.addOnePoint(points: incrementByOne())
    }
    
    func plusFivePoints() {
        let incrementByFive = point.incrementPoint(for: 5)
        self.gameProcessView?.addFivePoints(points: incrementByFive())
    }
    
    func plusTenPoints() {
        let incrementByTen = point.incrementPoint(for: 10)
        self.gameProcessView?.addTenPoints(points: incrementByTen())
    }
    
    func minusOnePoint() {
        let decrementByOne = point.incrementPoint(for: -1)
        self.gameProcessView?.minusOnePoint(points: decrementByOne())
    }
    
    func minusFivePoints() {
        let decrementByFive = point.decrementPoint(for: -5)
        self.gameProcessView?.minusFivePoints(points: decrementByFive())
    }
    
    func minusTenPoints() {
        let decrementByTen = point.decrementPoint(for: -10)
        self.gameProcessView?.minusTenPoints(points: decrementByTen())
    }
    
    func retrievePlayersFromDataBase() throws {
        do {
            let playerToAdd = try persistance?.retreive(object: Player.self, forKey: UserDefaults.CodingKeys.player.rawValue) ?? .init()
            players.append(playerToAdd)
        } catch {
           throw ErrorHandler.unableToDecode
        }
    }
}
