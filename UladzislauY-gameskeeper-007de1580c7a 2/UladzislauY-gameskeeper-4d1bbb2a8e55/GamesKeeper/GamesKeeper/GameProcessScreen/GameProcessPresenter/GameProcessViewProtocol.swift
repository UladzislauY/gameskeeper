//
//  GameProcessViewProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

protocol GameProcessViewProtocol: AnyObject {
    
    func resumeTimer()
    func pauseTimer()
    func openNewGameScreenModally()
    func openResultsScreen()
    func openRollScreen()
    func switchPlayerWithoutChangingScore()
    func returnToPreviousPlayerWithoutChangingScore()
    func undo()
    func addOnePoint(points: Int)
    func addFivePoints(points: Int)
    func addTenPoints(points: Int)
    func minusOnePoint(points: Int)
    func minusFivePoints(points: Int)
    func minusTenPoints(points:Int)
    func getPlayers()
}
