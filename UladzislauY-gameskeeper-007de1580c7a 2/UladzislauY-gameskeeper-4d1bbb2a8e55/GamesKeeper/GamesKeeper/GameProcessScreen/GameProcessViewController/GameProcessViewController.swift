//
//  GameProcessViewController.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import UIKit

class GameProcessViewController: UIViewController {
    
    //MARK: - Constants
    private enum Constant {
        
        static let screenTitle = "Game"
        static let plusOnePoint = "plusOnePoint"
        static let minusTenPoints = "minusTenPoints"
        static let minusFivePoints = "minusFivePoints"
        static let minusOnePoint = "minusOnePoint"
        static let plusFivePoints = "plusFivePoints"
        static let plusTenPoints = "plusTenPoints"
        static let newGameButton = "newGameButton"
        static let resultsButton = "resultsButton"
        
        enum Size {
            
            static let defaultValue: CGFloat = 0
            
            static let resumeStopStackViewStackViewSpacing: CGFloat = 8
            static let currentPlayerTurnStackViewSpacing: CGFloat = 25
            static let changeScoreButtonsStackViewSpacing: CGFloat = 18
            static let countdownLabelFontSize: CGFloat = 28
            
            static let currentPlayerCollectionViewItemWidth: CGFloat = 255
            static let currentPlayerCollectionViewItemHeight: CGFloat = 300
            
            static let rollDiceButtonBottomAnchor: CGFloat = -10
            static let rollDiceButtonRightAnchor: CGFloat = -20
            static let rollDiceButtonHeight: CGFloat = 30
            static let rollDiceButtonWidth: CGFloat = 30
            
            static let resumeStopStackViewTopAnchor: CGFloat = 30
            static let resumeStopStackViewLeftAnchor: CGFloat = 150
            static let resumeStopStackViewHeight: CGFloat = 50
            static let resumeStopStackViewRightAnchor: CGFloat = -120
            
            static let currentPlayerCollectionViewHeight: CGFloat = 301
            static let currentPlayerCollectionViewTopAnchor: CGFloat = 42
            static let currentPlayerCollectionViewLeftAnchor: CGFloat = 5
            static let currentPlayerCollectionViewRightAnchor: CGFloat = -5
            
            static let currentPlayerTurnStackViewTopAnchor: CGFloat = 20
            static let currentPlayerTurnStackViewLeftAnchor: CGFloat = 20
            static let currentPlayerTurnStackViewRightAnchor: CGFloat = -20
            static let currentPlayerTurnStackViewHeight: CGFloat = 80
            
            static let changeScoreButtonsStackViewTopAnchor: CGFloat = 22
            static let changeScoreButtonsStackViewLeftAnchor: CGFloat = 20
            static let changeScoreButtonsStackViewHeight: CGFloat = 60
            static let changeScoreButtonsStackViewRightAnchor: CGFloat = -20
            
            static let undoButtonWidth: CGFloat = 15
            static let undoButtonHeight: CGFloat = 20
            static let undoButtonTopAnchor: CGFloat = 22
            static let undoButtonLeftAnchor: CGFloat = 40
            static let undoButtonRightAnchor: CGFloat = -107
            
            static let playersListCollectionViewTopAnchor: CGFloat = 20
            static let playersListCollectionViewLeftAnchor: CGFloat = 107
            static let playersListCollectionViewBottonAnchor: CGFloat = -30
            static let playersListCollectionViewRightAnchor: CGFloat = -161
        }
        
        enum CollectionViewLayoutSize {
            
            static let currentPlayerCollectionViewItemWidth: CGFloat = 255
            static let currentPlayerCollectionViewItemHeight: CGFloat = 300
            
            static let playersListCollectionViewItemWidth: CGFloat = 15
            static let playersListCollectionViewItemHeight: CGFloat = 20
        }
    }
    //MARK: - Variables
    public var presenter: GameProcessViewPresenterProtocol!
   
    //MARK: - UserInterface Elements
    
    private lazy var newGameButton = NavigationBarButton(imageName: Constant.newGameButton, execute: presenter.pushToNewGameScreen)
    private lazy var resultsButton = NavigationBarButton(imageName: Constant.resultsButton, execute: presenter.pushResultsScreen)
    private lazy var resumeStopStackView = GenericStackView(arrangedSubviews: [countdownLabel, resumeStopTimerButton], distribution: .fillProportionally, spacing: Constant.Size.resumeStopStackViewStackViewSpacing, axis: .horizontal)
    private lazy var currentPlayerTurnStackView = GenericStackView(arrangedSubviews: [previousButton, plusOnePointButton, nextButton], distribution: .equalSpacing, spacing: Constant.Size.currentPlayerTurnStackViewSpacing, axis: .horizontal)
    private lazy var changeScoreButtonsStackView = GenericStackView(arrangedSubviews: [minusTenPointsButton, minusFivePointsButton, minusOnePointButton, plusFivePointsButton, plusTenPointsButton], distribution: .fillEqually, spacing: Constant.Size.changeScoreButtonsStackViewSpacing, axis: .horizontal)
    private lazy var plusOnePointButton = ChangeScoreButton(imageName: Constant.plusOnePoint, execute: presenter.plusOnePoint)
    private lazy var minusTenPointsButton = ChangeScoreButton(imageName: Constant.minusTenPoints, execute: presenter.minusTenPoints)
    private lazy var minusFivePointsButton = ChangeScoreButton(imageName: Constant.minusFivePoints, execute: presenter.minusFivePoints)
    private lazy var minusOnePointButton = ChangeScoreButton(imageName: Constant.minusOnePoint, execute: presenter.minusOnePoint)
    private lazy var plusFivePointsButton = ChangeScoreButton(imageName: Constant.plusFivePoints, execute: self.presenter.plusFivePoints)
    private lazy var plusTenPointsButton = ChangeScoreButton(imageName: Constant.plusTenPoints, execute: self.presenter.plusTenPoints)
    private lazy var rollDiceButton = ChangeScoreButton(imageName: Constants.Buttons.diceButton, execute: presenter.pushToRollDiceScreen)
    private lazy var resumeStopTimerButton = ChangeScoreButton(imageName: Constants.Buttons.play, execute: self.pinConstraints)
    private lazy var previousButton = ChangeScoreButton(imageName: Constants.Buttons.previousButton, execute: self.pinConstraints)
    private lazy var nextButton = ChangeScoreButton(imageName: Constants.Buttons.nextButton, execute: self.pinConstraints)
    private lazy var undoButton = ChangeScoreButton(imageName: Constants.Buttons.undo, execute: undo)
    private lazy var countdownLabel = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoBold, size: Constant.Size.countdownLabelFontSize) ?? UIFont(), textColor: .white, alignment: nil)
    private lazy var currentPlayerCollectionView = CurrentPlayerCollectionView(layout: .init(), dataSource: self, delegate: self, backgroundColor: UIColor.setCustomColor(color: .mainColor) ?? .label, cell: CurrentPlayerCollectionViewCell.self, cellIdentifier: CurrentPlayerCollectionViewCell.reuseIdentifier, layoutType: .horizontal(Constant.CollectionViewLayoutSize.currentPlayerCollectionViewItemWidth, Constant.CollectionViewLayoutSize.currentPlayerCollectionViewItemHeight))
    private lazy var playersListCollectionView = CurrentPlayerCollectionView(layout: .init(), dataSource: self, delegate: self, backgroundColor: UIColor.label, cell: PlayersListCollectionViewCell.self, cellIdentifier: PlayersListCollectionViewCell.reuseIdentifier, layoutType: .horizontal(Constant.CollectionViewLayoutSize.playersListCollectionViewItemWidth, Constant.CollectionViewLayoutSize.playersListCollectionViewItemHeight))
    private var subviews: [UIView] {return [playersListCollectionView, undoButton, changeScoreButtonsStackView, currentPlayerCollectionView, resumeStopStackView, currentPlayerTurnStackView]}
    //MARK: - ViewController Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constant.screenTitle
        self.view.backgroundColor = UIColor.setCustomColor(color: .mainColor)
        self.navigationController?.navigationBar.addSubview(rollDiceButton)
        self.subviews.forEach({view.addSubview($0)})
        self.pinConstraints()
        self.getPlayers()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
        
    }
    
    private func pinConstraints() {
        
        self.rollDiceButton.activateConstraintsOnView(top: nil, leading: nil, bottom: self.navigationController?.navigationBar.bottomAnchor, trailing: self.navigationController?.navigationBar.trailingAnchor, padding: .init(top: Constant.Size.defaultValue, left: Constant.Size.defaultValue, bottom: Constant.Size.rollDiceButtonBottomAnchor, right: Constant.Size.rollDiceButtonRightAnchor), size: .init(width: Constant.Size.rollDiceButtonWidth, height: Constant.Size.rollDiceButtonHeight))
        
        self.resumeStopStackView.activateConstraintsOnView(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.resumeStopStackViewTopAnchor, left: Constant.Size.resumeStopStackViewLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.resumeStopStackViewRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.resumeStopStackViewHeight))
        
        self.currentPlayerCollectionView.activateConstraintsOnView(top: self.resumeStopStackView.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.currentPlayerCollectionViewTopAnchor, left: Constant.Size.currentPlayerCollectionViewLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.currentPlayerCollectionViewRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.currentPlayerCollectionViewHeight))
        
        self.currentPlayerTurnStackView.activateConstraintsOnView(top: self.currentPlayerCollectionView.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.currentPlayerCollectionViewTopAnchor, left: Constant.Size.currentPlayerTurnStackViewLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.currentPlayerTurnStackViewRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.currentPlayerTurnStackViewHeight))
        
        self.changeScoreButtonsStackView.activateConstraintsOnView(top: self.currentPlayerTurnStackView.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.changeScoreButtonsStackViewTopAnchor, left: Constant.Size.changeScoreButtonsStackViewLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.changeScoreButtonsStackViewRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.changeScoreButtonsStackViewHeight))
        
        self.undoButton.activateConstraintsOnView(top: self.changeScoreButtonsStackView.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.playersListCollectionView.leadingAnchor, padding: .init(top: Constant.Size.undoButtonTopAnchor, left: Constant.Size.undoButtonLeftAnchor, bottom: Constant.Size.defaultValue, right: Constant.Size.undoButtonRightAnchor), size: .init(width: Constant.Size.undoButtonWidth, height: Constant.Size.undoButtonHeight))
        
        self.playersListCollectionView.activateConstraintsOnView(top: self.changeScoreButtonsStackView.bottomAnchor, leading: self.undoButton.trailingAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.playersListCollectionViewTopAnchor, left: Constant.Size.playersListCollectionViewLeftAnchor, bottom: Constant.Size.playersListCollectionViewBottonAnchor, right: Constant.Size.playersListCollectionViewRightAnchor), size: .init())
    }
    
    private func customizeNavigationBar() {
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.newGameButton)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.resultsButton)
        self.customizeNavigationBar(navigationController: self.navigationController ?? UINavigationController(), hasLargeTitle: true)
    }
    
    @objc func stopResumeTimer(_ sender: UIButton) {
        sender.isTouchInside == sender.isTouchInside ? sender.setImage(UIImage(named: Constants.Buttons.pause), for: .normal) : sender.setImage(UIImage(named: Constants.Buttons.play), for: .disabled)
    }
}
//MARK: - Extensions
extension GameProcessViewController: GameProcessViewProtocol {
    
    func getPlayers() {
        self.currentPlayerCollectionView.reloadData()
        self.playersListCollectionView.reloadData()
    }
    
    func addOnePoint(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func addFivePoints(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func addTenPoints(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func minusOnePoint(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func minusFivePoints(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func minusTenPoints(points: Int) {
        self.countdownLabel.text = String(points)
    }
    
    func resumeTimer() {
        
    }
    
    func pauseTimer() {
        
    }
    
    func openNewGameScreenModally() {
        self.presenter.pushToNewGameScreen()
    }
    
    func openResultsScreen() {
        self.presenter.pushResultsScreen()
    }
    
    func openRollScreen() {
        self.presenter.pushToRollDiceScreen()
    }
    
    func switchPlayerWithoutChangingScore() {
        
    }
    
    func returnToPreviousPlayerWithoutChangingScore() {
        
    }
    
    func undo() {
        
    }
}
//MARK: - UICollectionView Protocol Extensions
extension GameProcessViewController: UICollectionViewDelegate {
    
}

extension GameProcessViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
            
        case self.currentPlayerCollectionView:
            return presenter.players.count
            
        case self.playersListCollectionView:
            return presenter.players.count
            
        default:
            return Int()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
            
        case self.currentPlayerCollectionView:
            guard let currentPlayerCell = collectionView.dequeueReusableCell(withReuseIdentifier: CurrentPlayerCollectionViewCell.reuseIdentifier, for: indexPath) as? CurrentPlayerCollectionViewCell else {return UICollectionViewCell()}
            currentPlayerCell.populateCurrentPlayerCollectionViewCell(with: presenter.players[indexPath.item])
            return currentPlayerCell
            
        case self.playersListCollectionView:
            guard let playersListCell = collectionView.dequeueReusableCell(withReuseIdentifier: PlayersListCollectionViewCell.reuseIdentifier , for: indexPath) as? PlayersListCollectionViewCell else {return UICollectionViewCell()}
            playersListCell.populatePlayersListCollectionViewCell(with: presenter.players[indexPath.item])
            return playersListCell
            
        default:
            return UICollectionViewCell()
        }
    }
}
