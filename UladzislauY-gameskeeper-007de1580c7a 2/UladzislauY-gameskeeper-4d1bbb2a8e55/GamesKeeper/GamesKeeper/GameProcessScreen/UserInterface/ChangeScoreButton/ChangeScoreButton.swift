//
//  ChangeScoreButton.swift
//  GamesKeeper
//
//  Created by Владислав on 12/3/21.
//

import UIKit

class ChangeScoreButton: UIButton {
    
    private var execute: (() -> ())?
    
    init(imageName: String, execute: (() -> ())?) {
        super.init(frame: .zero)
        super.setImage(UIImage(named: imageName), for: .normal)
        super.translatesAutoresizingMaskIntoConstraints = false
        super.addTarget(self, action: #selector(activateTarget), for: .touchUpInside)
        self.execute = execute
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc private func activateTarget() {
        execute?()
    }
}
