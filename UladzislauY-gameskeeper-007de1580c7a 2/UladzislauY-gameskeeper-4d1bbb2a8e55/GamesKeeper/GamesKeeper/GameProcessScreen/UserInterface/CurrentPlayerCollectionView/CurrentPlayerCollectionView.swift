//
//  CurrentPlayerCollectionView.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import UIKit

enum CollectionViewLayout {
    
    case vertical(CGFloat, CGFloat)
    case horizontal(CGFloat, CGFloat)
}

class CurrentPlayerCollectionView: UICollectionView {
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(layout: UICollectionViewLayout, dataSource: UICollectionViewDataSource, delegate: UICollectionViewDelegate, backgroundColor: UIColor, cell: UICollectionViewCell.Type, cellIdentifier: String, layoutType: CollectionViewLayout) {
        self.init(frame: .zero, collectionViewLayout: .init())
        super.collectionViewLayout = self.configureCollectionViewLayout(layout: layoutType)
        super.dataSource = dataSource
        super.delegate = delegate
        super.backgroundColor = backgroundColor
        super.register(cell, forCellWithReuseIdentifier: cellIdentifier)
        
    }
    
    private func configureCollectionViewLayout(layout: CollectionViewLayout) -> UICollectionViewLayout {
        switch layout {
            
        case let .vertical(width, height):
            let verticalLayout = UICollectionViewFlowLayout()
            verticalLayout.scrollDirection = .vertical
            verticalLayout.itemSize = CGSize(width: width, height: height)
            return verticalLayout
            
        case let .horizontal(width, height):
            let horizontalLayout = UICollectionViewFlowLayout()
            horizontalLayout.scrollDirection = .horizontal
            horizontalLayout.itemSize = CGSize(width: width, height: height)
            return horizontalLayout
        }
    }
}
