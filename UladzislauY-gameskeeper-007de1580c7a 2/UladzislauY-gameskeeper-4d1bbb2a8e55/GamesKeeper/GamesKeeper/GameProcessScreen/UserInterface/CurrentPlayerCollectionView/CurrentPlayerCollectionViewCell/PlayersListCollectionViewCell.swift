//
//  PlayersListCollectionViewCell.swift
//  GamesKeeper
//
//  Created by admin on 29.11.21.
//

import UIKit

protocol PlayersListCollectionViewCellDelegate {
    
    func populatePlayersListCollectionViewCell(with model: Player)
}

class PlayersListCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Constants
    
    enum Constant {
        
        static let defaultValue: CGFloat = 0
        
        static let currentPlayerNameLabelTopAnchor: CGFloat = 1
        static let currentPlayerNameLabelLeftAnchor: CGFloat = 1
        static let currentPlayerNameLabelRightAnchor: CGFloat = -1
        static let currentPlayerNameLabelHeight: CGFloat = 24
        
        static let currentPlayerNameLabelFont: CGFloat = 20
    }
    
    //MARK: - UserInterface Elements
    
    lazy var playerName = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: Constant.currentPlayerNameLabelFont) ?? .init(), textColor: UIColor.colorize(r: 235, g: 235, b: 245, a: 1), alignment: .center)
    
    //MARK: - Variables
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    //MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    //MARK: - Cell Life Cycle Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
        self.addSubview(playerName)
        self.playerName.activateConstraintsOnView(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: Constant.currentPlayerNameLabelTopAnchor, left: Constant.currentPlayerNameLabelLeftAnchor, bottom: Constant.defaultValue, right: Constant.currentPlayerNameLabelRightAnchor), size: .init(width: Constant.defaultValue, height: Constant.currentPlayerNameLabelHeight))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.playerName.text = nil
    }
}

//MARK: - Extensions
extension PlayersListCollectionViewCell: PlayersListCollectionViewCellDelegate {
    
    func populatePlayersListCollectionViewCell(with model: Player) {
        guard let firstLetterOfPlayersFirstName = model.name?.first else {return}
        self.playerName.text = String(firstLetterOfPlayersFirstName)
    }
}
