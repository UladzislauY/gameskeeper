//
//  CurrentPlayerCollectionViewCell.swift
//  GamesKeeper
//
//  Created by admin on 23.11.21.
//

import UIKit

protocol CurrentPlayerCollectionViewCellProtocol {
    
    func populateCurrentPlayerCollectionViewCell(with model: Player)
}

class CurrentPlayerCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Constants
    private enum Constant {
        
        static let defaultValue: CGFloat = 0
        
        static let borderRadius: CGFloat = 15
        static let currentPlayerNameLabelTopAnchor: CGFloat = 20
        static let currentPlayerNameLabelLeftAnchor: CGFloat = 20
        static let currentPlayerNameLabelRightAnchor: CGFloat = -20
        static let currentPlayerNameLabelHeight: CGFloat = 40
        static let currentPlayerNameLabelFontSize: CGFloat = 28
        
        static let playerScoreLabelLeftAnchor: CGFloat = 50
        static let playerScoreLabelBottomAnchor: CGFloat = -100
        static let playerScoreLabelRightAnchor: CGFloat = -50
        static let playerScoreLabelHeight: CGFloat = 50
        static let playerScoreLabelFontSize: CGFloat = 41
    }
    //MARK: - UserInterface Elements
    lazy var currentPlayerName = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: Constant.currentPlayerNameLabelFontSize) ?? .init(), textColor: UIColor.colorize(r: 35, g: 35, b: 35, a: 1), alignment: .center)
    lazy var playerScoreLabel = GenericLabel(text: nil, font: UIFont(name: Constants.CustomFonts.nunitoBold, size: Constant.playerScoreLabelFontSize) ?? .init(), textColor: UIColor.colorize(r: 255, g: 253, b: 253, a: 1), alignment: .center)
    //MARK: - Variables
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    //MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = Constant.borderRadius
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: - Cell Life Cycle Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.setCustomColor(color: .currentPlayerCollectionViewColor)
        self.addSubview(currentPlayerName)
        self.addSubview(playerScoreLabel)
        
        self.currentPlayerName.activateConstraintsOnView(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: Constant.currentPlayerNameLabelTopAnchor, left: Constant.currentPlayerNameLabelLeftAnchor, bottom: Constant.defaultValue, right: Constant.currentPlayerNameLabelRightAnchor), size: .init(width: Constant.defaultValue, height: Constant.currentPlayerNameLabelHeight))
        
        self.playerScoreLabel.activateConstraintsOnView(top: nil, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: Constant.defaultValue, left: Constant.playerScoreLabelLeftAnchor, bottom: Constant.playerScoreLabelBottomAnchor, right: Constant.playerScoreLabelRightAnchor), size: .init(width: Constant.defaultValue, height: Constant.playerScoreLabelHeight))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.currentPlayerName.text = nil
        self.playerScoreLabel.text = nil 
    }
}
//MARK: - Extensions 
extension CurrentPlayerCollectionViewCell: CurrentPlayerCollectionViewCellProtocol {
    
    func populateCurrentPlayerCollectionViewCell(with model: Player) {
        self.currentPlayerName.text = model.name?.capitalized
        self.playerScoreLabel.text = "\(model.score?.score ?? Int())".capitalized
    }
}
