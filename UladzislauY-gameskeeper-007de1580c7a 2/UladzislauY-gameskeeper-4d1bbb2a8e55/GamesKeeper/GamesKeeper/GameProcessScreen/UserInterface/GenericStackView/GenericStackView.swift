//
//  GenericStackView.swift
//  GamesKeeper
//
//  Created by Владислав on 12/3/21.
//

import UIKit

class GenericStackView: UIStackView {
    
    init(arrangedSubviews: [UIView], distribution: UIStackView.Distribution, spacing: CGFloat, axis: NSLayoutConstraint.Axis) {
        super.init(frame: .zero)
        super.translatesAutoresizingMaskIntoConstraints = false
        super.distribution = distribution
        super.spacing = spacing
        super.axis = axis
        
        arrangedSubviews.forEach { (eachSubView) in
            super.addArrangedSubview(eachSubView)
        }
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
}
