//
//  GenericLabel.swift
//  GamesKeeper
//
//  Created by Владислав on 12/4/21.
//

import UIKit

class GenericLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        super.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(text: String?, font: UIFont, textColor: UIColor, alignment: NSTextAlignment?) {
        self.init(frame: .zero)
        super.text = text
        super.font = font
        super.textColor = textColor
        super.textAlignment = alignment ?? .center
    }
}

