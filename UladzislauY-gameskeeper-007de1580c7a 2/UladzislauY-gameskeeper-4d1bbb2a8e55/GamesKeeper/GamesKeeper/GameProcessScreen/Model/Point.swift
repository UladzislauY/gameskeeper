//
//  Point.swift
//  GamesKeeper
//
//  Created by Владислав on 12/3/21.
//

import Foundation

class Point: NSObject {
    
    func incrementPoint(for amount: Int) -> () -> Int {
        var total: Int = 0
        func incrementer() -> Int {
            total += amount
            return total
        }
        return incrementer
    }
    
    func decrementPoint(for amount: Int) -> () -> Int {
        var total: Int = 0
        func decrement() -> Int {
            total -= amount
            return total
        }
        return decrement
    }
}
