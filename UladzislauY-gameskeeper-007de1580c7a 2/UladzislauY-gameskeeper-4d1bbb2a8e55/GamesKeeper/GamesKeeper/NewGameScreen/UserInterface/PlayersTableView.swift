//
//  PlayersTableView.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import UIKit

class PlayersTableView: UITableView {
    
    private var cornerRadius: CGFloat = 15
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = cornerRadius
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    convenience init(style: UITableView.Style, color: UIColor, delegate: UITableViewDelegate, dataSource: UITableViewDataSource, cell: UITableViewCell.Type, cellIdentifier: String) {
        self.init(frame: .zero, style: style)
        super.backgroundColor = color
        super.delegate = delegate
        super.dataSource = dataSource
        super.register(cell, forCellReuseIdentifier: cellIdentifier)
    }
}
