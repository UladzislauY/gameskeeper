//
//  UIButton.swift
//  GamesKeeper
//
//  Created by Владислав on 11/20/21.
//

import UIKit

class StartGameButton: UIButton {
    
    private var execute: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        super.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(backgroudImage: String, execute: (() -> Void)?) {
        self.init(frame: .zero)
        super.setImage(UIImage(named: backgroudImage), for: .normal)
        super.addTarget(self, action: #selector(activateTarget), for: .touchUpInside)
        self.execute = execute
        
    }
    
    @objc private func activateTarget() {
        execute?()
    }
}
