//
//  ViewController.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import UIKit

class NewGameViewController: UIViewController{
    
    //MARK: - Constants
    
    private enum Constant {
        
        static let screenTitle = "Game Counter"
        static let playersTableViewHeaderTitle = "Players"
        static let playersTableViewFooterTitle = "Add Player"
        
        enum Size {
            static let defaultValue: CGFloat = 0
            static let heightForRow: CGFloat = 50
            static let addPlayerLabelForTableViewFooterFontSize: CGFloat = 16
            static let addPlayerFooterForPlayerTableViewSpacing: CGFloat = 8
            
            static let startGameButtonHeight: CGFloat = 65
            static let startGameButtonLeftAnchor: CGFloat = 20
            static let startGameButtonRightAnchor: CGFloat = -20
            static let startGameButtonBottomAnchor: CGFloat = -65
            
            static let playersTableViewTopAnchor: CGFloat = 25
            static let playersTableViewBottomAnchor: CGFloat = -25
            static let playersTableViewLeftAnchor: CGFloat = 20
            static let playersTableViewRightAnchor: CGFloat = -20
        }
    }
    //MARK: - UserInterface Elements
    
    private lazy var playersTableView = PlayersTableView(style: .insetGrouped, color: UIColor.setCustomColor(color: .mainColor) ?? .label, delegate: self, dataSource: self, cell: NewGameTableViewCell.self, cellIdentifier: NewGameTableViewCell.reuseIdentifier)
    private lazy var startGameButton = StartGameButton(backgroudImage: Constants.Image.startGameButton, execute: presenter.navigateToGameProcessScreen)
    private lazy var titleForPlayerTableView = GenericLabel(text: Constant.playersTableViewHeaderTitle, font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: Constant.Size.addPlayerLabelForTableViewFooterFontSize) ?? .init(), textColor: UIColor.colorize(r: 235, g: 235, b: 245, a: 0.6), alignment: .left)
    private lazy var addPlayerFooterForPlayerTableView = GenericStackView(arrangedSubviews: [addPlayerButtonFotTableViewFooter, addPlayerLabelForTableViewFooter], distribution: .fillProportionally, spacing: Constant.Size.addPlayerFooterForPlayerTableViewSpacing, axis: .horizontal)
    private lazy var addPlayerButtonFotTableViewFooter = ChangeScoreButton(imageName: Constants.Image.addButtonImage, execute: presenter.navigateToAddPlayerScreen)
    private lazy var addPlayerLabelForTableViewFooter = GenericLabel(text: Constant.playersTableViewFooterTitle, font: UIFont(name: Constants.CustomFonts.nunitoRegular, size: Constant.Size.addPlayerLabelForTableViewFooterFontSize) ?? .init(), textColor: UIColor.colorize(r: 235, g: 235, b: 245, a: 0.6), alignment: .right)
    //MARK: - Variables
    
    public var presenter: NewGameViewPresenterProtocol!
    private var isFirstLaunch: Bool = false
   
    //MARK: - ViewController Life Cycle Functions 
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = presenter.set(title: Constant.screenTitle)
        self.view.backgroundColor = UIColor.colorize(r: 35, g: 35, b: 35, a: 1)
        self.view.addSubview(startGameButton)
        self.view.addSubview(playersTableView)
        self.addConstraints()
        self.getPlayers()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar(isFirstLaunch)
        isFirstLaunch = true
        
    }
    
    private func addConstraints() {
        
        self.startGameButton.activateConstraintsOnView(top: nil, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.defaultValue, left: Constant.Size.startGameButtonLeftAnchor, bottom: Constant.Size.startGameButtonBottomAnchor, right: Constant.Size.startGameButtonRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.startGameButtonHeight))
        
        self.playersTableView.activateConstraintsOnView(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: self.startGameButton.topAnchor, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.playersTableViewTopAnchor, left: Constant.Size.startGameButtonLeftAnchor, bottom: Constant.Size.playersTableViewBottomAnchor, right: Constant.Size.playersTableViewRightAnchor), size: .init())
    }
    
    private func reloadTable() {
        DispatchQueue.main.async {
            self.playersTableView.reloadData()
        }
    }
}

//MARK: - Protocol Extensions
extension NewGameViewController: NewGameViewProtocol {
    func getPlayers() {
        try? self.presenter.retrievePlayersFromDataBase()
        self.reloadTable()
    }
    
    func receiveInputFromTextField(input: String) {
        let player: Player = Player(name: input, score: nil, players: [input])
        self.presenter.players.append(player)
        try? presenter.persistance.save(object: player, forKey: UserDefaults.CodingKeys.player.rawValue)
    }
    
    func setupTitleForNewGameView(title: String) -> String {
        let screenTitle = Constant.screenTitle
        self.title = screenTitle
        return screenTitle
    }
    
    @objc func startGame() {
        self.presenter.navigateToGameProcessScreen()
    }
    
    @objc func addPlayer() {
        self.presenter.navigateToAddPlayerScreen()
    }
}

extension NewGameViewController {
    
    private func customizeNavigationBar(_ isNotHidden: Bool) {
        guard isNotHidden else {
            return
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named: Constants.Buttons.cancelButton),
            style: .done,
            target: self,
            action: nil
        )
    }
}

//MARK: -  UITableView Extensions
extension NewGameViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.titleForPlayerTableView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.addPlayerFooterForPlayerTableView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.Size.heightForRow
    }
}

extension NewGameViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let playerCell = tableView.dequeueReusableCell(withIdentifier: NewGameTableViewCell.reuseIdentifier, for: indexPath) as? NewGameTableViewCell else {return UITableViewCell()}
        playerCell.configureNewGameTableViewCell(numeration: indexPath, with: presenter.players[indexPath.row])
        
        return playerCell
    }
}
