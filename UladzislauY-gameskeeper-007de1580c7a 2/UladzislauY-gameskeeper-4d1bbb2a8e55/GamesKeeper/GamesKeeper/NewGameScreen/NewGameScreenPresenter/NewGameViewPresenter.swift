//
//  NewGameViewPresenter.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import Foundation

protocol NewGameViewPresenterProtocol: AnyObject {
    
    func set(title: String) -> String
    func navigateToAddPlayerScreen()
    func navigateToGameProcessScreen()
    func givenInput(input: String)
    func retrievePlayersFromDataBase() throws
    var players: [Player] {get set}
    var persistance: Persisting {get set}
}
