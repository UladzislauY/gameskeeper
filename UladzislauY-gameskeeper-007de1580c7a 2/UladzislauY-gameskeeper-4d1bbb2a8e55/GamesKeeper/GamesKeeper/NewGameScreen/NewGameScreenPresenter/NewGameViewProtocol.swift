//
//  NewGameViewProtocol.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import Foundation

protocol NewGameViewProtocol: AnyObject {
    
    func setupTitleForNewGameView(title: String) -> String
    func startGame()
    func addPlayer()
    func receiveInputFromTextField(input: String)
    func getPlayers()
}
