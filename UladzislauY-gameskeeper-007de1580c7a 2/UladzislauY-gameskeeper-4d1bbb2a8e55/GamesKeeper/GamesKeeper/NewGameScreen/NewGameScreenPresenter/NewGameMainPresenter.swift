//
//  NewGameMainPresenter.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import Foundation


class NewGameMainPresenter: NewGameViewPresenterProtocol {
    
    weak public var newGameView: NewGameViewProtocol?
    public var playerModel: Player
    public var router: RouterProtocol?
    public var persistance: Persisting
    public var players: [Player] = []
    
    init(view: NewGameViewProtocol, model: Player, router: RouterProtocol, persistance: Persisting) {
        
        self.newGameView = view
        self.playerModel = model
        self.router = router
        self.persistance = persistance
    }
    
    func set(title: String) -> String {
        self.newGameView?.setupTitleForNewGameView(title: title) ?? ""
    }
    
    func navigateToAddPlayerScreen() {
        self.router?.navigate(with: .addPlayerScreen)
    }
    
    func navigateToGameProcessScreen() {
        self.router?.navigate(with: .gameProcessScreen)
    }
    
    func givenInput(input: String) {
        self.playerModel.add(player: input)
        self.newGameView?.receiveInputFromTextField(input: input)
    }
    
    func retrievePlayersFromDataBase() throws {
        do {
            let player = try persistance.retreive(object: Player.self, forKey: UserDefaults.CodingKeys.player.rawValue) 
            self.players.append(player)
        } catch {
           throw ErrorHandler.unableToDecode
        }
    }
}
