//
//  Player.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import Foundation

struct Player: Codable {
    
    var name: String? = ""
    var score: Result? = nil 
    var players: [String] = []
    
    mutating func add(player: String) {
        self.players.append(player)
        self.name = player
    }
    
    mutating func deletePlayer() {
        self.players.removeLast()
    }
}
