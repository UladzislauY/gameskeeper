//
//  PersistableProtocol.swift
//  GamesKeeper
//
//  Created by admin on 25.11.21.
//

import Foundation

protocol Persistable {
    
    func setObject<T: Encodable>(_ object: T, forKey: String) throws
    func getObject<T: Decodable>(forKey: String, castTo type: T.Type) throws -> T
}

extension UserDefaults: Persistable {
    
    func setObject<T>(_ object: T, forKey: String) throws where T : Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            self.set(data, forKey: forKey)
        }catch _ {
            throw ErrorHandler.unableToEncode
        }
    }
    
    func getObject<T>(forKey: String, castTo type: T.Type) throws -> T where T : Decodable {
        guard let data = data(forKey: forKey) else {throw ErrorHandler.noValue}
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        }catch _ {
            throw ErrorHandler.unableToDecode
        }
    }
}

extension UserDefaults {
    
    enum CodingKeys: String {
        case player = "player"
        
        var stringRepresentation: String? {
            rawValue
        }
    }
}
