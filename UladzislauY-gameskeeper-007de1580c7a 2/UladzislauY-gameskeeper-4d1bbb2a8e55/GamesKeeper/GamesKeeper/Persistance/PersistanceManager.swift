//
//  PersistanceManager.swift
//  GamesKeeper
//
//  Created by admin on 25.11.21.
//

import Foundation

protocol Persisting {
    
    var persistance: UserDefaults {get set}
    func save<T: Codable>(object: T, forKey key: String) throws
    func retreive<T: Codable>(object: T.Type, forKey key: String) throws -> T
}

final class PersistanceManager: Persisting {
    
    private init() {}
    static let shared = PersistanceManager()
    
    var persistance = UserDefaults.standard
    
    func save<T: Codable>(object: T, forKey key: String) throws {
        do {
            try persistance.setObject(object, forKey: key)
        }catch {
            throw ErrorHandler.unableToEncode
        }
    }
    
    func retreive<T: Codable>(object: T.Type, forKey key: String) throws -> T {
        do {
            let objectToRetrieve = try persistance.getObject(forKey: key, castTo: T.self)
            return objectToRetrieve
        }catch {
            throw ErrorHandler.unableToDecode
        }
    }
}
