//
//  ErrorHandler.swift
//  GamesKeeper
//
//  Created by admin on 29.11.21.
//

import Foundation

public enum ErrorHandler: String, LocalizedError {
    
    case unableToEncode = "Unable to encode object into data"
    case noValue = "No data object found for the given key"
    case unableToDecode = "Unable to decode object into given type"
    
    public var errorDescription: String? {
        rawValue
    }
}
