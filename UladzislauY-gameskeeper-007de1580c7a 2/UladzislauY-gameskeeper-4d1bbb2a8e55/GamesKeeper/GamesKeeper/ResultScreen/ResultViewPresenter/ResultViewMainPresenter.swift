//
//  ResultViewMainPresenter.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

class ResultViewMainPresenter: ResultViewPresenterProtocol {
  
    weak var resultView: ResultViewProtocol?
    let playerModel: Player
    var router: RouterProtocol?
    var players: [Player] = []
    
    init(resultView: ResultViewProtocol, playerModel: Player, router: RouterProtocol) {
        self.resultView = resultView
        self.playerModel = playerModel
        self.router = router
    }
    
    func pushToNewGameViewController() {
        self.router?.navigate(with: .addPlayerScreen)
    }
    
    func goBackToGameProcessViewController() {
        self.router?.navigate(with: .gameProcessScreen)
    }
}
