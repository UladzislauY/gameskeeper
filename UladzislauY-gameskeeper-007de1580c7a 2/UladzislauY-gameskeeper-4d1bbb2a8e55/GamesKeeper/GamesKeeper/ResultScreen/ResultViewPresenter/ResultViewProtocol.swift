//
//  ResultViewProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

protocol ResultViewProtocol: AnyObject {
    
    func openNewGameScreen()
    func popToGameProcessScreen()
}
