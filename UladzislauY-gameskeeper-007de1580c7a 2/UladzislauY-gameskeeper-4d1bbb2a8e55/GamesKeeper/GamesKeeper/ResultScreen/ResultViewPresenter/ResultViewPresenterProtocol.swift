//
//  ResultViewPresenterProtocol.swift
//  GamesKeeper
//
//  Created by admin on 22.11.21.
//

import Foundation

protocol ResultViewPresenterProtocol: AnyObject {
    
    func pushToNewGameViewController()
    func goBackToGameProcessViewController()
    var players: [Player] {get set}
}
