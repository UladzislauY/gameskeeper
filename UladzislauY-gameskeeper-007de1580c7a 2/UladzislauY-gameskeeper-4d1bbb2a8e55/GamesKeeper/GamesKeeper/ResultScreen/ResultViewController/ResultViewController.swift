//
//  ResultViewController.swift
//  GamesKeeper
//
//  Created by Владислав on 11/18/21.
//

import UIKit

class ResultViewController: UIViewController {
    //MARK: - Constants
    private enum Constant {
        
        static let screenTitle = "Results"
        static let newGameButton = "newGameButton"
        static let resumeGameButton = "resumeButton"
        
        enum Size {
            static let defaultValue: CGFloat = 0
            static let resultsTableViewHeight: CGFloat = 470
            static let resultsTableViewLeftAnchor: CGFloat = 20
            static let resultsTableViewRightAnchor: CGFloat = -20
            static let resultsTableViewBottonAnchor: CGFloat = -20
        }
    }
    
    //MARK: - UserInterface Elements
    private lazy var resultsTableView = PlayersTableView(style: .insetGrouped, color: UIColor.setCustomColor(color: .mainColor) ?? .label, delegate: self, dataSource: self, cell: ResultTableViewCell.self, cellIdentifier: ResultTableViewCell.reuseIdentifier)
    private lazy var newGameButton = NavigationBarButton(imageName: Constant.newGameButton, execute: openNewGameScreen)
    private lazy var resumeGameButton = NavigationBarButton(imageName: Constant.resumeGameButton, execute: popToGameProcessScreen)
    //MARK: - Variables
    public var presenter: ResultViewPresenterProtocol!
    
    //MARK: - ViewController Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constant.screenTitle
        self.view.backgroundColor = UIColor.colorize(r: 35, g: 35, b: 35, a: 1)
        self.view.addSubview(resultsTableView)
        self.pinConstraints()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }
    
    private func pinConstraints() {
        self.resultsTableView.activateConstraintsOnView(top: nil, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor, padding: .init(top: Constant.Size.defaultValue, left: Constant.Size.resultsTableViewLeftAnchor, bottom: Constant.Size.resultsTableViewBottonAnchor, right: Constant.Size.resultsTableViewRightAnchor), size: .init(width: Constant.Size.defaultValue, height: Constant.Size.resultsTableViewHeight))
    }
    
    private func customizeNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.newGameButton)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.resumeGameButton)
        self.customizeNavigationBar(navigationController: self.navigationController ?? UINavigationController(), hasLargeTitle: true)
    }
}

//MARK: - Extensions
extension ResultViewController: ResultViewProtocol {
    
    @objc func openNewGameScreen() {
        self.presenter.pushToNewGameViewController()
    }
    
    @objc func popToGameProcessScreen() {
        self.presenter.goBackToGameProcessViewController()
    }  
}

extension ResultViewController: UITableViewDelegate {
    
}

extension ResultViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let resultCell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewCell.reuseIdentifier, for: indexPath) as? ResultTableViewCell else {return UITableViewCell()}
        return resultCell
    }
}
